# Practice Vocabulary Data App
This application is one of the two applications of the application set 'Practice Vocabulary'

The purpose of this practice-vocabulary-data-ap is to provide intentional and flexible apis to CRUD(Create, Retrieve, Update & Delete) operations on vocabulary.


## Features:
* A quiz kind of exprience to practice vocabulary.
* You can get a random word and see whether you know it or not.
* You can repeat the random words to make sure you have learnt the word.
* You can mark certain words as LEARNED so to avoid repetitions.

## Dev Setup
Make sure you have NODEJS, NPM and GIT installed on your mechine

```
$ git clone git@gitlab.com:sravanrekandar/practice-vocabulary-data-app.git
$ cd practice-vocabulary-data-app
$ npm install
$ npm start
```
the app would start on http://localhost:3333


## APIs
Notes : 
* For now, all calls are provided in GET
* Possible usable links are provided with each api. This feature helps the data layer development fast and testing easy. All we need to do is install [JSON View](https://chrome.google.com/webstore/detail/jsonview/chklaanhfefbnpoihckbnefhakgolnmc) kind of plugin in your browser


### Home
```
http://localhost:3333
```
This api provides the usage info and some links

#### Response
```
{
  success: true,
  message: " Welcome to 'Practice Vocabulary Data App'. This service provides you the CRUD functionalities to learn vocabulary. You can add new words along with the definitions and examples. You can search words. TODO: A quiz kind of exprience to practice vocabulary. You can get a random word and see whether you know it or not. You can mark certain words as LEARNED. You can repeat the random words to make sure you have learnt the word. ",
  __links: {
    dataHome: "http://localhost:3333",
    allWords: "http://localhost:3333/words",
    randomWord: "http://localhost:3333/words/getRandomWord",
    addWord: "http://localhost:3333/words/createWord?word={"word":"","definitions":[],"examples":[]}"
  }
}
```

### All Words
```
http://localhost:3333/words
```
This api returns all the available words and their relative links

#### Response
```
{
  "success": true,
  "message": "All Available Words : 5 words found",
  "words": [
    {
      "word": "deem",
      "link": "http://localhost:3333/words?search=deem"
    },
    {
      "word": "plucky",
      "link": "http://localhost:3333/words?search=plucky"
    },
    {
      "word": "rile up",
      "link": "http://localhost:3333/words?search=rile up"
    },
    {
      "word": "corral",
      "link": "http://localhost:3333/words?search=corral"
    },
    {
      "word": "up",
      "link": "http://localhost:3333/words?search=up"
    }
  ],
  "__links": {
    "dataHome": "http://localhost:3333",
    "allWords": "http://localhost:3333/words",
    "randomWord": "http://localhost:3333/words/getRandomWord",
    "addWord": "http://localhost:3333/words/createWord?word={\"word\":\"\",\"definitions\":[],\"examples\":[]}"
  }
```
### Search Word
```
http://localhost:3333/words?search=up

{
  "success": true,
  "message": "Search results for \"up\": 2 word(s) found.",
  "words": [
    {
      "word": "rile up",
      "definitions": [
        "to annoy s.b"
      ],
      "examples": [
        ""
      ]
    },
    {
      "word": "up",
      "definitions": [
        "Above",
        "increase"
      ],
      "examples": [
        ""
      ]
    }
  ],
  "__links": {
    "dataHome": "http://localhost:3333",
    "allWords": "http://localhost:3333/words",
    "randomWord": "http://localhost:3333/words/getRandomWord",
    "addWord": "http://localhost:3333/words/addWord?word={\"word\":\"\",\"definitions\":[],\"examples\":[]}"
  }
}
```


### Get Random Word
```
http://localhost:3333/words/getRandomWord

{
  "success": true,
  "message": "Random Word",
  "word": {
    "word": "deem",
    "definitions": [
      "consider"
    ],
    "examples": [
      "the strike was considered to be illegal"
    ]
  },
  "__links": {
    "dataHome": "http://localhost:3333",
    "allWords": "http://localhost:3333/words",
    "randomWord": "http://localhost:3333/words/getRandomWord",
    "addWord": "http://localhost:3333/words/createWord?word={\"word\":\"\",\"definitions\":[],\"examples\":[]}"
  }
}
```

### Add word
```
http://localhost:3333/words/createWord?word={"word":"","definitions":[],"examples":[]}
```

This api responds with the status true/ false based on the completion of operation: 
1. If incorrect data is supplied: Instructions to use the api
2. If trying to add an existing word: responds with all existing matches
3. If trying to add an existing word with option 'forceReplace: true': adds the word and responds with success message
4. If trying to add a new word: Adds a new word and responds with a success message

#### Example responses

1. If incorrect data is supplied: Instructions to use the api
```
http://localhost:3333/words/addWord?word={"word":"","definitions":[],"examples":[]}
```

```
{
  "success": false,
  "message": "\n        Please make sure you are sending data in correct format.\n        Stringify the word object and send as query.\n        Look at the exampleUrl with this response.\n      ",
  "exampleWord": {
    "word": "love",
    "definitions": [
      "noun - a strong feeling of affection.",
      "verb - feel deep affection or sexual love for (someone)."
    ],
    "examples": [
      "babies fill parents with intense feelings of love",
      "do you love me?"
    ]
  },
  "exampleUrl": "http://localhost:3333/words/createWord?word={\"word\":\"love\",\"definitions\":[\"noun - a strong feeling of affection.\",\"verb - feel deep affection or sexual love for (someone).\"],\"examples\":[\"babies fill parents with intense feelings of love\",\"do you love me?\"]}",
  "__links": {
    "dataHome": "http://localhost:3333",
    "allWords": "http://localhost:3333/words",
    "randomWord": "http://localhost:3333/words/getRandomWord",
    "addWord": "http://localhost:3333/words/createWord?word={\"word\":\"\",\"definitions\":[],\"examples\":[]}"
  }
}
```

2. If trying to add an existing word: responds with all existing matches
```
http://localhost:3333/words/addWord?word={"word":"deem","definitions":[],"examples":[]}
```

```
{
  "success": false,
  "message": "The word \"deem\" exists. To replace, Please use .forceReplace:true in the word object",
  "existingWord": {
    "word": "deem",
    "definitions": [
      "consider"
    ],
    "examples": [
      "the strike was considered to be illegal"
    ]
  },
  "newWord": {
    "word": "deem",
    "definitions": [],
    "examples": []
  },
  "__links": {
    "dataHome": "http://localhost:3333",
    "allWords": "http://localhost:3333/words",
    "randomWord": "http://localhost:3333/words/getRandomWord",
    "addWord": "http://localhost:3333/words/addWord?word={\"word\":\"\",\"definitions\":[],\"examples\":[]}"
  }
}
```

3. Replcing the exising word
```
http://localhost:3333/words/addWord?word={"word":"deem","definitions":["considering to do something"],"examples":[], "forceReplace": true}
```

```
{
  "success": false,
  "message": "The word \"deem\" exists. To replace, Please use .forceReplace:true in the word object",
  "existingWord": {
    "word": "deem",
    "definitions": [
      "consider"
    ],
    "examples": [
      "the strike was considered to be illegal"
    ]
  },
  "newWord": {
    "word": "deem",
    "definitions": [],
    "examples": []
  },
  "__links": {
    "dataHome": "http://localhost:3333",
    "allWords": "http://localhost:3333/words",
    "randomWord": "http://localhost:3333/words/getRandomWord",
    "addWord": "http://localhost:3333/words/addWord?word={\"word\":\"\",\"definitions\":[],\"examples\":[]}"
  }
}
```

4. Adding new word
```
http://localhost:3333/words/addWord?word={"word":"development","definitions":["the process of developing or being developed."],"examples":["she traces the development of the novel"]}
```

```
{
  "success": true,
  "message": "Successfully added new word \"development\"",
  "word": {
    "word": "development",
    "definitions": [
      "the process of developing or being developed."
    ],
    "examples": [
      "she traces the development of the novel"
    ]
  },
  "__links": {
    "dataHome": "http://localhost:3333",
    "allWords": "http://localhost:3333/words",
    "randomWord": "http://localhost:3333/words/getRandomWord",
    "addWord": "http://localhost:3333/words/addWord?word={\"word\":\"\",\"definitions\":[],\"examples\":[]}"
  }
}
```
