/* eslint-disable no-underscore-dangle */
const util = require('util')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
const logger = require('../utils/logger')
const db = require('../db/dbConnect')
const WordModel = require('../db/models/word')

function initiateRoutes(app) {
  function getWordByQuery(query, cb) {
    WordModel.find(query, (error, words) => {
      if (error) {
        logger.error('Could not find word by query: ', util.inspect(query), util.inspect(error))
        cb({
          success: false,
          err: error,
        })
      } else {
        logger.info('Serving the matching words', util.inspect(words))
        cb({
          success: true,
          words,
        })
      }
    })
  }
  app.post('/word', jsonParser, (req, res) => {
    logger.info(req.method.toUpperCase(), req.path)
    logger.info('req.body = ', util.inspect(req.body))
    req.body.word.lastUpdated = new Date()
    req.body.word.lastUpdatedBy = req.user || 'mongoose'
    const newWord = new WordModel(req.body.word)

    newWord.save((err, result) => {
      if (err) {
        logger.error('Error in creating new word', util.inspect(err.errors))
        res.json({ success: false, err: err.errors })
      } else {
        logger.info('Word saved successfully!', util.inspect(result))
        res.json({ success: true, word: result })
      }
    })
  })
  app.put('/word', jsonParser, (req, res) => {
    logger.info(req.method.toUpperCase(), req.path)
    logger.info('req.body = ', util.inspect(req.body))
    const updatingWord = req.body.word
    const _id = updatingWord._id
    delete updatingWord._id
    delete updatingWord.__v
    logger.info('Updating word before saving ', util.inspect(updatingWord))
    updatingWord.lastUpdated = new Date()
    updatingWord.lastUpdatedBy = req.user || 'mongoose'
    WordModel.findByIdAndUpdate(_id, { $set: updatingWord, $inc: { __v: 1 } }, { new: true }, (error, word) => {
      if (error) {
        logger.error('Error updating word ', util.inspect(error))
        res.json({ success: false, err: error })
      } else {
        logger.info(`Successfully updated the word "${word.wordText}"`)
        res.json({
          success: true,
          word,
        })
      }
    })
  })
  app.delete('/word/:wordId', (req, res) => {
    logger.info(req.method.toUpperCase(), req.path)
    const _id = req.params.wordId
    if (!_id) {
      logger.info('_id not available to delete')
      res.json({
        success: false,
        err: 'Provide proper word id to delete',
      })
      return
    }
    WordModel.findByIdAndRemove(_id, (error) => {
      if (error) {
        const errMessage = `Could not delete word with id ${_id}`
        logger.info(errMessage)
        res.json({
          success: false,
          err: errMessage,
        })
      } else {
        res.json({
          success: true,
        })
      }
    })
  })
  app.get('/word?', (req, res) => {
    logger.info(req.method.toUpperCase(), req.path)
    const query = req.query
    if (Object.keys(query) === 0) {
      const errMessage = 'No query provided to search a word'
      logger.error(errMessage)
      res.json({
        success: false,
        err: errMessage,
      })
      return
    }
    getWordByQuery(query, (response) => {
      res.json(response)
    })
  })
  app.get('/words', (req, res) => {
    logger.info(req.method.toUpperCase(), req.path)
    const query = {}
    getWordByQuery(query, (response) => {
      res.json(response)
    })
  })
}

function init(app) {
  db.once('open', () => {
    logger.info('DB connection open. Initiating Word routes')
    initiateRoutes(app)
  })
}
module.exports = {
  init,
}
