/* eslint-disable no-underscore-dangle */
const util = require('util')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
const logger = require('../utils/logger')
const db = require('../db/dbConnect')
const WordSetModel = require('../db/models/wordSet')
function initiateRoutes(app) {
  app.post('/wordSet', jsonParser, (req, res) => {
    logger.info(req.method.toUpperCase(), req.path)
    logger.info('req.body = ', util.inspect(req.body))
    req.body.wordSet.lastUpdated = new Date()
    req.body.wordSet.lastUpdatedBy = req.user || 'mongoose'
    let newWordSet
    try {
      newWordSet = new WordSetModel(req.body.wordSet)
    } catch (err) {
      logger.info('Trouble creating a word set : ', util.inspect(err))
      res.json({
        success: false,
        err: `Trouble creating word set : ${err.toString()}`,
      })
      return
    }
    newWordSet.save((err, result) => {
      if (err) {
        logger.error('Error in creating new word set', util.inspect(err.errors))
        res.json({ success: false, err: err.errors })
      } else {
        logger.info('Word Set saved successfully!', util.inspect(result))
        res.json({ success: true, wordSet: result })
      }
    })
  })
  app.put('/wordSet', jsonParser, (req, res) => {
    logger.info(req.method.toUpperCase(), req.path)
    logger.info('req.body = ', util.inspect(req.body))
    const updatingWordSet = req.body.wordSet
    const _id = updatingWordSet._id
    delete updatingWordSet._id
    delete updatingWordSet.__v

    updatingWordSet.lastUpdated = new Date()
    updatingWordSet.lastUpdatedBy = req.user || 'mongoose'
    logger.info('Updating word set before saving: ', util.inspect(updatingWordSet))
    WordSetModel.findOneAndUpdate({ _id }, { $set: updatingWordSet, $inc: { __v: 1 } }, { upsert: false, new: true }, (error, wordSet) => {
      if (error) {
        logger.error('Error updating word set ', util.inspect(error))
        res.json({ success: false, err: error })
      } else {
        logger.info(`Successfully updated the word set "${wordSet}"`)
        res.json({
          success: true,
          wordSet,
        })
      }
    })
  })
  app.delete('/wordSet/:wordSetId', (req, res) => {
    logger.info(req.method.toUpperCase(), req.path)
    const _id = req.params.wordSetId
    if (!_id) {
      logger.info('_id not available to delete')
      res.json({
        success: false,
        err: 'Provide proper word set id to delete',
      })
      return
    }
    WordSetModel.findByIdAndRemove(_id, (error) => {
      if (error) {
        const errMessage = `Could not delete word set with id ${_id}`
        logger.info(errMessage)
        res.json({
          success: false,
          err: errMessage,
        })
      } else {
        res.json({
          success: true,
        })
      }
    })
  })
  app.get('/wordSet/:wordSetId', (req, res) => {
    logger.info(req.method.toUpperCase(), req.path)
    const _id = req.params.wordSetId
    if (!_id) {
      logger.info('_id not available to get word set')
      res.json({
        success: false,
        err: 'Provide proper word set id to get word set',
      })
      return
    }
    WordSetModel.findOne({ _id }, (error1, wordSet) => {
      if (error1) {
        logger.error('Could not find word by query: ', util.inspect(query), util.inspect(error))
        res.json({
          success: false,
          err: error1,
        })
      } else {
        logger.info('Serving the word set', util.inspect(wordSet))
        res.json({
          success: true,
          wordSet,
        })
      }
    })
  })

  app.get('/getAllWordSetsCompact', (req, res) => {
    logger.info(req.method.toUpperCase(), req.path)
    WordSetModel.find({}, (error, result) => {
      if (error) {
        logger.error('Touble pulling word sets', util.inspect(error))
        res.json({
          sucess: false,
          err: error,
        })
      } else {
        const compactWordSets = result.map(e => ({ _id: e._id, name: e.name }))
        res.json({
          success: true,
          wordSets: compactWordSets,
        })
      }
    })
  })
}
function init(app) {
  db.once('open', () => {
    logger.info('DB connection open. Initiating Word Set routes')
    initiateRoutes(app)
  })
}

module.exports = {
  init,
}
