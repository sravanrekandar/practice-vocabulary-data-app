/*
eslint global-require: 0*/
const util = require('util')
const wordRoutes = require('./word')
const wordSetRoutes = require('./wordSet')
const logger = require('../utils/logger')

function routes(app) {
  logger.info('Initiating routes')
  wordRoutes.init(app)
  wordSetRoutes.init(app)

  app.get('/', (req, res) => {
    logger.info('Request received for : /')
    const response = {
      success: true,
      message: `
        Welcome to 'Practice Vocabulary Data App'.
        This service provides you the CRUD functionalities to learn vocabulary.
        You can add new words along with the definitions and examples.
        You can search words.
      `,
    }
    logger.log('Serving response', util.inspect(response))
    res.json(response)
  })
}

module.exports = routes
