const util = require('util')
const { MongoClient, ObjectId } = require('mongodb')
const { wordType } = require('./types/word')
const logger = require('./utils/logger')

const url = 'mongodb://localhost:27017/practice-vocabulary'
let words
let users

function getWordsFromDB(cb) {
  logger.info('Getting words from database')
  MongoClient.connect(url, (err, db) => {
    if (err) {
      logger.error('Failed to conect DB')
      db.close()
      cb({
        success: false,
        err,
      })
      return
    }

    db.collection('words').find({}).toArray((parseErr, docs) => {
      db.close()
      if (parseErr) {
        logger.error('Failed to parse the array')
        cb({
          success: false,
          err: parseErr,
        })
        return
      }

      logger.log('Serving the words: ', util.inspect(docs))
      words = docs // Updating the common variable
      cb({
        success: true,
        words: docs,
      })
    })
  })
}
function searchWord(wordText, cb) {
  logger.info(`searching the word ${wordText}`)
  MongoClient.connect(url, (err, db) => {
    if (err) {
      logger.error('Failed to conect DB')
      db.close()
      cb({
        success: false,
        err,
      })
      return
    }

    db.collection('words').find({ wordText: { $regex: new RegExp(wordText, 'gi') } }).toArray((dbError, resWords) => {
      if (dbError) {
        logger.error(`Some problem processing the request: findOne: ${wordText}`, util.inspect(dbError))
      }
      cb({
        success: true,
        words: resWords,
      })
    })
  })
}

function addWordToDB(word, cb) {
  MongoClient.connect(url, (err, db) => {
    // DB connection failed
    if (err) {
      db.close()
      cb({
        success: false,
        err,
      })
      return
    }

    logger.log('Connected successfully to server')
    db.collection.insert(word, (dbError) => {
      db.close()
      if (dbError) {
        cb({
          success: false,
          err: dbError,
        })
        return
      }

      cb({
        success: true,
      })
    })
  })
}
function getUsersFromDB(cb) {
  MongoClient.connect(url, (err, db) => {
    // DB connection failed
    if (err) {
      db.close()
      cb({
        success: false,
        err,
      })
    }

    winston.log('Connected successfully to server')
    db.collection('users').find({}).toArray((parseErr, docs) => {
      db.close()
      // Failed to parse the users to array
      if (parseErr) {
        return cb({
          success: false,
          err: parseErr,
        })
      }

      users = docs // Updating the common variable
      return cb({
        success: true,
        users: docs,
      })
    })
  })
}

function fetchWords(cb) {
  // If words are already available, no need to fetch from DB
  if (words) {
    logger.log('Serving the words from cache')
    cb({
      success: true,
      words,
    })
  } else {
    getWordsFromDB(cb)
  }
}

function addWord(word, cb) {
  if (!wordType.test(word)) {
    cb({
      success: false,
      err: 'Word TYPE mismatch',
    })
    return
  }
  addWordToDB(word, cb)
}
function fetchUsers(cb) {
  // If words are already available, no need to fetch from DB
  if (users) {
    cb({
      success: true,
      users,
    })
  } else {
    getUsersFromDB(cb)
  }
}

function fetchUser(userId, cb) {
  MongoClient.connect(url, (err, db) => {
    winston.log('connected to fetch the user')
    // DB connection failed
    if (err) {
      cb({
        success: false,
        err,
      })
    }

    winston.log('Connected successfully to server')
    db.collection('users').findOne({ _id: ObjectId(userId) }, (parseErr, user) => {
      // Failed to parse the users to array
      if (parseErr) {
        cb({
          success: false,
          err: parseErr,
        })
        return
      }

      cb({
        success: true,
        user,
      })
    })

    db.close()
  })
}

module.exports = {
  fetchWords,
  searchWord,
  fetchUsers,
  fetchUser,
}
