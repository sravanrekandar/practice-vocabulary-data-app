const util = require('util')
const async = require('async')
const { MongoClient, ObjectId } = require('mongodb')
const logger = require('../utils/logger')
const { AppEnv } = require('../../app-config')
const url = AppEnv.dbPath

function findUser(username, cb) {
  logger.info('Finding user ', username)
  setImmediate(() => {
    if (username === 'sravanrekandar') {
      cb({
        success: true,
        user: {
          username: 'sravanrekandar',
          password: 'rekandar',
        },
      })
    } else {
      cb({
        success: false,
        err: `Incorrect username ${username}`,
      })
    }
  })
}

module.exports = {
  findUser,
}
