const mongoose = require('mongoose')
const logger = require('../utils/logger')
const { AppEnv } = require('../../app-config')
mongoose.Promise = global.Promise
mongoose.connect(AppEnv.dbPath)
const db = mongoose.connection
db.on('error', logger.error.bind(logger, 'connection error:'))
module.exports = db
