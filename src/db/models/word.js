const mongoose = require('mongoose')
const { Schema } = mongoose
const definitionTypeEnum = {
  values: [
    'VERB',
    'ADJECTIVE',
    'NOUN',
    'PHRASEL VERB',
    'PREPOSITION',
    'ADVERB',
    'CONJUNCTION',
    'PRONOUN',
  ],
  message: 'enum validator failed for path `{PATH}` with value `{VALUE}`',
}
const otherLanguages = {
  telugu: String,
  tamil: String,
  kannada: String,
  malayalam: String,
  odiya: String,
  bengali: String,
  assami: String,
  gujarathi: String,
  hindi: String,
  marata: String,
}
const definition = {
  definitionType: { type: String, enum: definitionTypeEnum, required: true },
  definitionText: { type: String, required: true },
  examples: [String],
  synonyms: [String],
  otherLanguages,
}
const word = {
  wordText: { type: String, required: true },
  phoneticScript: { type: String, required: true },
  syllable: { type: String, required: true },
  definitions: [definition],
  tags: [String],
  authors: [String],
  lastUpdated: { type: String, required: true },
  lastUpdatedBy: { type: String, required: true },
}

const wordSchema = Schema(word)
wordSchema.pre('save', function (next) {
  const currentDate = new Date()
  this.lastUpdated = currentDate
  next()
})

wordSchema.methods.getDescription = function () {
  return `${this.wordText.toUpperCase()} has ${this.definitions.length} definition(s)`
}
wordSchema.statics.findByWordText = function (wordText, cb) {
  return this.find({ name: new RegExp(wordText, 'ig') }, cb)
}

const Word = mongoose.model('Word', wordSchema)
module.exports = Word
