const mongoose = require('mongoose')
const { Schema } = mongoose

const wordSet = {
  name: { type: String, required: true },
  description: { type: String, default: 'N/A' },
  words: [
    {
      _id: { type: String, required: true },
      wordText: { type: String, required: true },
    },
  ],
  authors: [String],
  lastUpdated: { type: String, required: true },
  lastUpdatedBy: { type: String, required: true },
}

const wordSetSchema = Schema(wordSet)
wordSetSchema.pre('save', function (next) {
  const currentDate = new Date()
  this.lastUpdated = currentDate
  next()
})

const WordSet = mongoose.model('WordSet', wordSetSchema)
module.exports = WordSet
