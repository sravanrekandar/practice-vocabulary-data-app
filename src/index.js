const https = require('https')
const os = require('os')
const fs = require('fs')
const bodyParser = require('body-parser')
const express = require('express')
const cors = require('cors')

const logger = require('./utils/logger')
const routes = require('./routes')
const { AppEnv } = require('../app-config')
const { DEFAULT_PORT } = AppEnv
const app = express()
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.set('port', (process.env.PORT || DEFAULT_PORT))

const appConstants = {
  PORT: app.get('port'),
}

routes(app, appConstants)
https.createServer({
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem'),
}, app).listen(app.get('port'), (err) => {
  if (err) {
    logger.error(err)
  }
  logger.info(`Practice-vocabulary-data-app listening on https://${os.hostname()}:${app.get('port')}`)
})

module.exports = app
