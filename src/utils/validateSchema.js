module.exports = (schema, input) => {
  let status
  try {
    status = schema.validate(input)
  } catch (err) {
    status = err.toString()
  }

  if (!status) {
    return {
      success: true,
    }
  }
  return {
    success: false,
    err: status,
  }
}
