const MongoClient = require('mongodb').MongoClient
const assert = require('assert')
const winston = require('winston')

const url = 'mongodb://localhost:27017/practice-vocabulary'

function seedWords(words) {
  if (!Array.isArray(words)) {
    winston.error('Words is not a valid data to seed. Skipping inserting words')
  }
  MongoClient.connect(url, (err, db) => {
    assert.equal(null, err)
    winston.log('Connected successfully to server')

    const collection = db.collection('words')
    // Insert words
    words.forEach((w) => {
      collection.insertOne(w, (error, r) => {
        assert.equal(null, err)
        assert.equal(1, r.insertedCount)
        db.close()
        winston.log('word seeded successfully')
      })
    })
  })
}

function seedUsers(users) {
  if (!Array.isArray(users)) {
    winston.error('Users is not a valid data to seed. Skipping inserting users')
  }
  MongoClient.connect(url, (err, db) => {
    assert.equal(null, err)
    winston.log('Connected successfully to server')

    const collection = db.collection('users')
    // Insert all words
    collection.insertMany(users, (error, result) => {
      assert.equal(error, null)
      assert.equal(users.length, result.result.n)
      assert.equal(users.length, result.ops.length)
      winston.log(`Inserted ${result.result.n} users into the collection`)
      db.close()
    })
  })
}

function clearWords() {
  MongoClient.connect(url, (err, db) => {
    assert.equal(null, err)
    winston.log('Connected successfully to server')
    const words = db.collection('words')
    words.deleteMany({})
    winston.log('Words cleared successfully')
    db.close()
  })
}

function clearUsers() {
  MongoClient.connect(url, (err, db) => {
    assert.equal(null, err)
    winston.log('Connected successfully to server')
    const users = db.collection('users')
    users.deleteMany({})
    winston.log('Users cleared successfully')
    db.close()
  })
}

module.exports = {
  seedWords,
  seedUsers,
  clearWords,
  clearUsers,
}
