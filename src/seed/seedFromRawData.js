/*
eslint-disable import/no-dynamic-require, global-require, no-param-reassign
*/
const fs = require('fs')
const path = require('path')
const _ = require('lodash')
const dataUtils = require('../dataUtils')
const seedToDB = require('./seedToDB')

const { getWordSetsByWords } = dataUtils
const MARKS = ['MASTERED', 'MASTERED_1', 'REVIEWED', 'REVIEWED_1', 'REVIEWED_2', 'LEARNING', 'NEWWORD']
const rawWordSetsPath = path.resolve(__dirname, './assets/wordSets')

const files = fs.readdirSync(rawWordSetsPath)

let allWords = files.map((fileName) => {
  if (fileName.startsWith('.')) {
    return []
  }
  return require(`${rawWordSetsPath}/${fileName}`)
})
allWords = [].concat.apply([], allWords)
allWords.reverse()
allWords.forEach((w) => {
  delete w.lastEdited
  w.wordText = w.wordText.trim().toLowerCase()
  w.lastUpdated = Date.now()
  w.lastUpdatedBy = 'seed process'
  if (w.tags.length === 0) {
    w.tags.push('basic')
  }
  w.tags = w.tags.map(t => t.toLowerCase())
})
const wordSets = getWordSetsByWords(allWords)

// Removing duplicates from wordSets
Object.keys(wordSets).forEach((setName) => {
  const set = wordSets[setName]
  const wordTexts = _.uniq(set.words.map(e => e.wordText))
  set.words = wordTexts.map(wordText => set.words.find(w => w.wordText === wordText))
  set.length = set.words.length
})

const users = ['sravan', 'shiva']
const userData = users.map((name) => {
  const sets = _.cloneDeep(wordSets)
  Object.keys(sets).forEach((set) => {
    sets[set].words = sets[set].words.map(w => ({
      wordText: w.wordText,
      mark: MARKS[Math.floor(Math.random() * MARKS.length)],
      lastUpdated: Date.now(),
    }))
  })
  return {
    name,
    regionalLanguage: 'telugu',
    wordSets: sets,
  }
})
// words after removing duplicates
let words = []
Object.keys(wordSets).forEach((setName) => {
  words = words.concat(wordSets[setName].words)
})

seedToDB.clearWords()
// seedToDB.clearUsers()
seedToDB.seedWords(words)
// seedToDB.seedUsers(userData)

// const seededDirPath = path.resolve(__dirname, '../../data')
// fs.writeFileSync(`${seededDirPath}/words.json`, JSON.stringify(words))
// fs.writeFileSync(`${seededDirPath}/userData.json`, JSON.stringify(userData))
// console.log('Data seeded: words.json & userData.json')
