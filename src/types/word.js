/* eslint-disable no-underscore-dangle */
const _ = require('lodash')
const { createSchema } = require('json-gate')

const WORD_TYPES = [
  'VERB',
  'ADJECTIVE',
  'NOUN',
  'PHRASEL VERB',
  'PREPOSITION',
  'ADVERB',
  'CONJUNCTION',
  'PRONOUN',
]

const otherLanguagesType = {
  type: 'object',
  required: true,
  additionalProperties: false,
  properties: {
    telugu: { type: 'string' },
    tamil: { type: 'string' },
    kannada: { type: 'string' },
    malayalam: { type: 'string' },
    odiya: { type: 'string' },
    bengali: { type: 'string' },
    assami: { type: 'string' },
    gujarathi: { type: 'string' },
    hindi: { type: 'string' },
    marata: { type: 'string' },
  },
}
const definitionType = {
  type: 'object',
  properties: {
    definitionType: {
      type: 'string',
      enum: WORD_TYPES,
      required: true,
    },
    definitionText: {
      type: 'string',
      minLength: 1,
      required: true,
    },
    examples: {
      type: 'array',
      items: [
        {
          type: 'string',
        },
      ],
      minItems: 1,
      required: true,
    },
    synonyms: {
      type: 'array',
      items: [
        {
          type: 'string',
        },
      ],
    },
    otherLanguages: otherLanguagesType,
  },
}

const wordType = {
  type: 'object',
  additionalProperties: false,
  properties: {
    _id: {
      type: 'string',
    },
    wordText: { type: 'string', required: true },
    phoneticScript: { type: 'string', required: true },
    syllable: { type: 'string', required: true },
    definitions: {
      type: 'array',
      items: [
        definitionType,
      ],
      minItems: 1,
      required: true,
    },
    tags: {
      type: 'array',
      items: [
        { type: 'string' },
      ],
      minItems: 1,
      required: true,
    },
    lastUpdated: {
      type: ['string', 'number'],
      required: true,
    },
    lastUpdatedBy: {
      type: 'string',
      required: true,
    },
    authors: {
      type: 'array',
      minItems: 1,
      items: [
        { type: 'string' },
      ],
      required: true,
    },
  },
}

const wordCreateType = _.cloneDeep(wordType)
delete wordCreateType._id

module.exports = {
  definitionType,
  wordType,
  wordCreateType,
  otherLanguagesType,
  wordSchema: createSchema(wordType),
  wordCreateSchema: createSchema(wordCreateType),
  definitionSchema: createSchema(definitionType),
  otherLanguagesSchema: createSchema(otherLanguagesType),
}
