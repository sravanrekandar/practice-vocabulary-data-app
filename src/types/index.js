const user = require('./user')
const word = require('./word')

module.exports = {
  user,
  word,
}
