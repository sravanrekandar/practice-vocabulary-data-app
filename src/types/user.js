const ducktype = require('ducktype')

const wordType = ducktype({
  wordText: String,
  mark: String,
  lastUpdated: Number,
})
const wordSetType = ducktype({
  name: String,
  wordsCount: Number,
  words: [wordType],
})

const userType = ducktype({
  name: String,
  regionalLanguage: String,
  wordSets: [wordSetType],
})

module.exports = {
  wordType,
  wordSetType,
  userType,
}
