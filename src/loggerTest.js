const wordSchema = require('./types/word').wordSchema

const input = {
  wordText: 'audacity',
  phoneticScript: '/ɔːˈdasɪti/',
  syllable: 'aw-das-i-tee',
  definitions: [
    {
      definitionType: 'ADJECTIVE',
      definitionText: 'difficult to understand; obscure.',
      examples: ['an abstruse philosophical inquiry'],
      otherLanguages: {
        telugu: 'చిక్కైన',
      },
    },
  ],
  tags: ['sometag'],
  lastUpdated: Date.now().toString(),
  lastUpdatedBy: 'sravan',
  authors: ['sravan'],
}

let status

try {
  status = wordSchema.validate(input)
} catch (err) {
  status = err.toString()
}

console.log('---->', status)
