const config = {
  development: {
    dbPath: 'mongodb://localhost:27017/practice-vocabulary',
    DEFAULT_PORT: 3333,
  },
  test: {
    dbPath: 'mongodb://localhost:27018/practice-vocabulary',
    DEFAULT_PORT: 4444,
  },
  production: {
    dbPath: 'mongodb://localhost:27019/practice-vocabulary',
    DEFAULT_PORT: 5555,
  },
}

const AppEnv = config[process.env.NODE_ENV] || config.development
module.exports = {
  AppEnv,
}
