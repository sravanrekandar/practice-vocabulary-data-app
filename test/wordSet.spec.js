/* eslint-disable no-underscore-dangle
 */
process.env.NODE_ENV = 'test'
const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../src/index')
chai.should()
const WordSetModel = require('../src/db/models/wordSet')
chai.use(chaiHttp)

describe('WordSet', () => {
  beforeEach((done) => { // Before each test we empty the database
    WordSetModel.remove({}, () => {
      done()
    })
  })
  describe('/POST wordSet', () => {
    it('it should not POST a wordSet without name field', (done) => {
      const wordSet = {
        authors: [],
        words: [],
      }
      chai.request(app)
        .post('/wordSet')
        .send({ wordSet })
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('success').eql(false)
          res.body.should.have.property('err')
          done()
        })
    })
    it('it should create a wordSet ', (done) => {
      const wordSet = {
        name: 'basic 1',
        authors: ['sravan', 'shiva'],
        words: [{ _id: 1, wordText: 'fear' }],
        definitions: [],
      }
      chai.request(app)
        .post('/wordSet')
        .send({ wordSet })
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('success').eql(true)
          res.body.should.have.property('wordSet')
          res.body.wordSet.should.have.property('name').eql('basic 1')
          done()
        })
    })
  })

  describe('/PUT/:id wordSet', () => {
    it('it should UPDATE a wordSet given the id', (done) => {
      const wordSet = new WordSetModel({
        name: 'basic 1',
        words: [],
        authors: [],
        lastUpdated: new Date(),
        lastUpdatedBy: 'sravan',
      })
      wordSet.save()

      chai.request(app)
        .put('/wordSet/')
        .send({ wordSet: { _id: wordSet._id, name: 'basic 1', authors: ['sravan'], words: [] } })
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.deep.property('wordSet.authors[0]', 'sravan')
          done()
        })
    })
  })
  describe('/DELETE/:id word', () => {
    it('it should DELETE a word set given the id', (done) => {
      const wordSet = new WordSetModel({
        name: 'basic 1',
        words: [],
        authors: [],
        lastUpdated: new Date(),
        lastUpdatedBy: 'sravan',
      })
      wordSet.save()

      wordSet.save((err, savedWord) => {
        chai.request(app)
          .delete(`/wordSet/${savedWord._id}`)
          .end((error, res) => {
            res.should.have.status(200)
            res.body.should.be.an('object')
            res.body.should.have.property('success', true)
            WordSetModel.find({}, (error1, result) => {
              chai.expect(result).to.have.lengthOf(0)
              done()
            })
          })
      })
    })
  })
  describe('/GET word sets', () => {
    it('it should GET all available word sets', (done) => {
      const wordSet = new WordSetModel({
        name: 'basic 1',
        words: [],
        authors: [],
        lastUpdated: new Date(),
        lastUpdatedBy: 'sravan',
      })
      wordSet.save()
      chai.request(app)
        .get(`/wordSet/${wordSet._id}`)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('success').to.equal(true)
          res.body.should.have.deep.property('wordSet.name').eql('basic 1')
          done()
        })
    })
  })
  describe('/GET getAllWordSetsCompact', () => {
    it('it should GET all available word sets in compact mode', (done) => {
      const wordSet1 = new WordSetModel({
        name: 'basic 1',
        words: [],
        authors: [],
        lastUpdated: new Date(),
        lastUpdatedBy: 'sravan',
      })
      wordSet1.save()
      const wordSet2 = new WordSetModel({
        name: 'basic 2',
        words: [],
        authors: ['sravan '],
        lastUpdated: new Date(),
        lastUpdatedBy: 'sravan',
      })
      wordSet2.save()
      const expectedResult = [
        { _id: wordSet1._id, name: wordSet1.name },
        { _id: wordSet2._id, name: wordSet2.name },
      ]

      chai.request(app)
        .get('/getAllWordSetsCompact')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('success').to.equal(true)
          res.body.should.have.property('wordSets').to.have.lengthOf(2)
          res.body.should.have.deep.property('wordSets[0]._id').eql(expectedResult[0]._id.toString())
          res.body.should.have.deep.property('wordSets[0].name').eql(expectedResult[0].name)
          res.body.should.have.deep.property('wordSets[1]._id').eql(expectedResult[1]._id.toString())
          res.body.should.have.deep.property('wordSets[1].name').eql(expectedResult[1].name)
          done()
        })
    })
  })
})
