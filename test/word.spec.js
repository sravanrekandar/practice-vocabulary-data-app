/* eslint-disable no-underscore-dangle
 */
process.env.NODE_ENV = 'test'
const chai = require('chai')
const async = require('async')
const chaiHttp = require('chai-http')
const app = require('../src/index')
chai.should()
const WordModel = require('../src/db/models/word')
chai.use(chaiHttp)

describe('Word', () => {
  beforeEach((done) => { // Before each test we empty the database
    WordModel.remove({}, () => {
      done()
    })
  })
  describe('/POST word', () => {
    it('it should not POST a word without wordText field', (done) => {
      const word = {
        phoneticScript: '..',
      }
      chai.request(app)
        .post('/word')
        .send({ word })
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('success').eql(false)
          res.body.should.have.property('err')
          done()
        })
    })
    it('it should create a word ', (done) => {
      const word = {
        wordText: 'fear',
        phoneticScript: '/fɪə/',
        syllable: '/fɪə/',
        authors: ['sravan', 'shiva'],
        tags: ['basic', 'emotion'],
        definitions: [],
      }
      chai.request(app)
        .post('/word')
        .send({ word })
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('success').eql(true)
          res.body.should.have.property('word')
          res.body.word.should.have.property('wordText').eql('fear')
          done()
        })
    })
  })
  describe('/PUT/:id word', () => {
    it('it should UPDATE a word given the id', (done) => {
      const word = new WordModel({
        wordText: 'fear',
        phoneticScript: '/fɪə/',
        syllable: '/fɪə/',
        authors: ['sravan', 'shiva'],
        tags: ['basic', 'emotion'],
        definitions: [],
        lastUpdated: new Date(),
        lastUpdatedBy: 'sravan',
      })
      word.save()

      chai.request(app)
        .put('/word/')
        .send({ word: { _id: word._id, tags: ['new tag'] } })
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.deep.property('word.tags[0]', 'new tag')
          done()
        })
    })
  })
  describe('/DELETE/:id word', () => {
    it('it should DELETE a word given the id', (done) => {
      const word = new WordModel({
        wordText: 'fear',
        phoneticScript: '/fɪə/',
        syllable: '/fɪə/',
        authors: ['sravan', 'shiva'],
        tags: ['basic', 'emotion'],
        definitions: [],
        lastUpdated: new Date(),
        lastUpdatedBy: 'sravan',
      })

      word.save((err, savedWord) => {
        chai.request(app)
          .delete(`/word/${savedWord._id}`)
          .end((error, res) => {
            res.should.have.status(200)
            res.body.should.be.an('object')
            res.body.should.have.property('success', true)
            WordModel.find({}, (error1, result) => {
              chai.expect(result).to.have.lengthOf(0)
              done()
            })
          })
      })
    })
  })
  describe('/GET words', () => {
    it('it should GET all the words', (done) => {
      chai.request(app)
        .get('/words')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.an('object')
          res.body.should.have.property('success').to.equal(true)
          res.body.should.have.property('words').to.have.lengthOf(0)
          done()
        })
    })
  })
  describe('/GET/word?', () => {
    it('it should GET a word by the given query', (done) => {
      const word = new WordModel({
        wordText: 'fear',
        phoneticScript: '/fɪə/',
        syllable: '/fɪə/',
        authors: ['sravan', 'shiva'],
        tags: ['basic', 'emotion'],
        definitions: [],
        lastUpdated: new Date(),
        lastUpdatedBy: 'sravan',
      })
      word.save((err, savedWord) => {
        chai.request(app)
          .get(`/word?wordText=fear`)
          .end((error, res) => {
            res.should.have.status(200)
            res.body.should.be.a('object')
            res.body.should.have.property('success').eql(true)
            res.body.should.have.property('words').to.have.lengthOf(1)
            res.body.should.have.deep.property('words[0].wordText', 'fear')
            res.body.should.have.deep.property('words[0]._id', savedWord._id.toString())
            done()
          })
      })
    })
    it('it should GET matching words by the given query', (done) => {
      const word1 = new WordModel({
        wordText: 'fear',
        phoneticScript: '/fɪə/',
        syllable: '/fɪə/',
        authors: ['sravan', 'shiva'],
        tags: ['basic', 'emotion'],
        definitions: [],
        lastUpdated: new Date(),
        lastUpdatedBy: 'sravan',
      })
      const word2 = new WordModel({
        wordText: 'fear',
        phoneticScript: '/fɪə-less/',
        syllable: '/fɪə-less/',
        authors: ['sravan', 'shiva'],
        tags: ['basic', 'emotion', 'advanced'],
        definitions: [],
        lastUpdated: new Date(),
        lastUpdatedBy: 'sravan',
      })
      async.waterfall([
        function saveWord1(ayncCB) {
          word1.save((err, savedWord1) => {
            ayncCB(null, savedWord1)
          })
        },
        function saveWord2(savedWord1, ayncCB) {
          word2.save((err, savedWord2) => {
            ayncCB(null, savedWord1, savedWord2)
          })
        },
      ], (error, savedWord1, savedWord2) => {
        chai.request(app)
          .get(`/word?wordText=fear`)
          .end((apiError, res) => {
            res.should.have.status(200)
            res.body.should.be.a('object')
            res.body.should.have.property('success').eql(true)
            res.body.should.have.property('words').to.have.lengthOf(2)
            res.body.should.have.deep.property('words[0].wordText', 'fear')
            res.body.should.have.deep.property('words[0]._id', savedWord1._id.toString())
            res.body.should.have.deep.property('words[1].wordText', 'fear')
            res.body.should.have.deep.property('words[1]._id', savedWord2._id.toString())
            done()
          })
      })
    })
  })
})
