const md2json = require('md-to-json')
const gulp = require('gulp')

gulp.task('md2json_task', () => {
  return gulp.src('./src/seed/assets/md/word1.md')
  .pipe(md2json())
  .pipe(gulp.dest('.'))
})
